# Ages Produktwarnungen & Rückrufe Website Scraper

Hole die Produktwarnungen und Rückrufe der Ages für Österreich um sie auf Mastodon zur Verfügung zu stellen.

en: scrape the product warnings and return calls of Ages for Austria to make them available on Mastodon

Mastodon Account: https://aut.social/@ages_rueckruf_inofficial
