#!/usr/bin/env python3
import json
import re
import requests
import shutil
import os

# mastodon python tutorial
# https://shkspr.mobi/blog/2018/08/easy-guide-to-building-mastodon-bots/
# pip install Mastodon.py
from mastodon import Mastodon

def scrape_products():
    """scrape the ages website for product return warnings"""
    r = requests.get("https://www.ages.at/mensch/produktwarnungen-produktrueckrufe")
    html = r.text
    # write or load local cache file, useful for debugging
    #with open("site.html", "w", encoding="utf-8") as f:
    #    f.write(html)
    #with open("site.html", "r", encoding="utf-8") as f:
    #    html = f.read()
    re_section_elements = '(?:<section class="product-item">)(.*?)(?:</section>)'
    matches = re.findall(re_section_elements, html, re.DOTALL)

    def parse_product(product_html : str):
        """extract information about the product from the product html"""
        product_header = re.search('<h2 class="heading-underline">(.*?)</h2>', product_html)[1]
        product_category = re.search('<p><span>(.*?)</span><span>', product_html)[1]
        product_datetime = re.search('<time datetime="(.*?)">', product_html)[1]

        prod_info_rows_html = re.findall('(?:<div class="product-info-row">)(.*?)(?:\t</div>)', product_html, re.DOTALL)
        def parse_product_info_row(product_info_row_html):
            key = re.search('<div class="product-info-row-left">(.*?)</div>', product_info_row_html)[1]
            val = re.search('<div class="product-info-row-right">(.*?)</div>', product_info_row_html)[1]
            return (key, val.strip())
        prod_info_rows_list = [parse_product_info_row(elem) for elem in prod_info_rows_html]
        prod_info_rows = {key: val for key, val in prod_info_rows_list}
        #print(prod_info_rows)

        btn_detail = re.findall('(?:<div class="wrapper-btn-detail">)(.*?)(?:\t</div>)', product_html, re.DOTALL)
        details_link = re.search('<a href="(.*?)" class="btn btn-primary"', btn_detail[0])[1]

        prod_info_known_keys = [
            "Rückrufgrund",
            "Grund",
            "In Verkehr gebracht von",
            "Hersteller",
            "Ablaufdatum",
            "Chargennummer",
        ]
        # show info about unknown keys, for me to add to the message
        for key in prod_info_rows.keys():
            if key not in prod_info_known_keys:
                print("product: '{}': unknown product key '{}'".format(product_header, key))
        # no need to load the image, commented out
        #product_image_div = re.findall('(?:<div class="product-image">)(.*?)(?:\t</div>)', product_html, re.DOTALL)
        #product_image_url = re.search('<a href="(.*?)"', product_image_div[0])[1]
        #product_image_copyright = re.search('<span>&copy;[ ]*(.*?)</span>', product_image_div[0])[1]
        #product_image_filename = product_image_url.split("/")[-1]

        ret = {
            "header": product_header,
            "category": product_category,
            "datetime": product_datetime,
            "return_reason": prod_info_rows.get("Rückrufgrund", None),
            "reason": prod_info_rows.get("Grund", None),
            "seller": prod_info_rows.get("In Verkehr gebracht von", None),
            "producer": prod_info_rows.get("Hersteller", None),
            "expiration_date": prod_info_rows.get("Ablaufdatum", None),
            "charge_number": prod_info_rows.get("Chargennummer", None),
            "details_link": details_link,
            #"product_image_url": product_image_url,
            #"product_image_filename": product_image_filename,
            #"product_image_copyright": product_image_copyright,
        }
        if ret["return_reason"] is None and ret["reason"] is None:
            raise RuntimeError("product: '{}' has neither 'Rückrufgrund' nor 'Grund' key: {}".format(
                product_header,
                prod_info_rows,
            ))
        return ret

    products = [parse_product(product) for product in matches]
    return products

def product_to_id(product):
    """little helper to make the fact we're using the link as duplication-prevention id
    more explicit"""
    return product["details_link"]

def mastodon_status_to_id(status_content):
    """for deduplication use the details link url as ID
    extract from mastodon status content entry"""
    url_match = re.search('href="https://www.ages.at(.*?)"', status_content)
    if url_match is None:
        return None
    url = url_match[1]
    return url

def product_to_mastodon_status_text(product):
    """format the text for the post to mastodon"""
    ret = "#RÜCKRUF | #WARNUNG | {header} | {datetime} |".format(**product)
    ret +="\nKategorie: {category}".format(**product)
    def append_if_set(ret, key, label):
        if product[key] is not None:
            ret += ("\n{label}: {value}".format(label=label, value=product[key]))
        return ret
    ret = append_if_set(ret, "return_reason", "Rückrufgrund")
    ret = append_if_set(ret, "reason", "Grund")
    ret = append_if_set(ret, "producer", "Hergestellt von")
    ret = append_if_set(ret, "expiration_date", "Ablaufdatum")
    ret = append_if_set(ret, "charge_number", "Chargennummer")

    ret +="\nDetails: https://www.ages.at{details_link}".format(**product)
    #ret +="\nimage copyright: © {product_image_copyright}".format(**product)
    return ret

def mastodon_post(mastodon, product):
    status_text = product_to_mastodon_status_text(product)

    # we don't have the copyright to the images, so don't down/upload them
    #r = requests.get("https://www.ages.at{}".format(product["product_image_url"]), stream=True)
    #print("downloading image: https://www.ages.at{}".format(product["product_image_url"]))
    #if r.status_code == 200:
    #    print("downloaded image, posting ...")
    #    cached_file = os.path.join("cache", product["product_image_filename"])
    #    with open(cached_file, 'wb') as f:
    #        r.raw.decode_content = True
    #        shutil.copyfileobj(r.raw, f)
    #    media = mastodon.media_post(
    #        cached_file,
    #        description="© " + product["product_image_copyright"])
    #    mastodon.status_post(status_text, media_ids=[media])
    #else:
    #    print("error downloading image, posting just text")
    mastodon.status_post(status_text)

def mastoton_process(mastodon, products, mastodon_status_ids):
    """check posts for deduplication, if unique post the product as status message"""
    if not os.path.isdir("cache"):
        os.mkdir("cache")
    products_to_post = []
    for product in products:
        product_id = product_to_id(product)
        if product_id in mastodon_status_ids:
            print("product '{}' already posted, assuming all older are posted as well".format(
                product["header"]))
            break
        else:
            products_to_post.append(product)
    # post producs in oldest post first
    products_to_post.reverse()
    for product in products_to_post:
        print("posting product: ", product["header"])
        mastodon_post(mastodon, product)

# get a list of products from ages website
# currently the latest 10 products are shown
products = scrape_products()
# write or load local cache file, useful for debugging
#with open("products.json", "w", encoding="utf-8") as f:
#    json.dump(products, f, indent=2, ensure_ascii=False)
#with open("products.json", "r", encoding="utf-8") as f:
#    products = json.load(f)

#  Set up Mastodon
mastodon = Mastodon(
    access_token = 'token.secret',
    api_base_url = 'https://aut.social/'
)

# get the already posted product returns
me_id = mastodon.me()
statuses = mastodon.account_statuses(me_id)
status_contents = [s["content"] for s in statuses]
mastodon_status_ids_all = [mastodon_status_to_id(s) for s in status_contents]
mastodon_status_ids = [s for s in mastodon_status_ids_all if s is not None]

# start posting!
mastoton_process(mastodon, products, mastodon_status_ids)

print("Finished!")
